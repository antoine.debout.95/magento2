<?php
namespace Debout\Contesthk\Block;

class Subscription extends \Magento\Framework\View\Element\Template{
    
    /**
     * Construct method
     *
     * @author Antoine DEBOUT
     * @date 11/06/2020
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        array $data = []
    )
    {
        parent::__construct($context, $data);
    }

    /**
     * Form Action
     * Get url for post subscription
     * @author Antoine DEBOUT
     * @date 11/06/2020
     * @return string 
     */
    public function getSubscritionformurlAction(){
        return '/contesthk/subscription/add';
    }
}