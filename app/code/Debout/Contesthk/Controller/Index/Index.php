<?php
namespace Debout\Contesthk\Controller\Index;

class Index extends \Magento\Framework\App\Action\Action{
    
    /**
     * Execute function
     * Display module home page
     * 
     * @author Antoine DEBOUT
     * @date 11/06/2020
     */
    public function execute(){
        $this->_view->loadLayout();
        $this->_view->renderLayout();
    }
}