<?php

namespace Debout\Contesthk\Controller\Subscription;

use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Mail\Template\TransportBuilder;

class Add extends \Magento\Framework\App\Action\Action
{
    protected $_transportBuilder;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        TransportBuilder $transportBuilder
    ){
        $this->_transportBuilder = $transportBuilder;
        parent::__construct($context);
    }
    /**
     * Subscription add action
     *
     * @author Antoine DEBOUT
     * @date 11/06/2020
     * @return void
     */
    public function execute()
    {
        $post = (array) $this->getRequest()->getPost();

        if (!empty($post)) {
            $newsletter =  (isset($post['newsletter'])) ? 1 : 0;
            $sms = (isset($post['sms'])) ? 1 : 0;
            $rulesacceptation =  (isset($post['rulesacceptation'])) ? 1 : 0;;
            
            $subscription = $this->_objectManager->create('Debout\Contesthk\Model\Contesthk');
            $subscription->setName($post['name']);
            $subscription->setFirstname($post['firstname']);
            $subscription->setPhone($post['phone']);
            $subscription->setEmail($post['email']);
            $subscription->setNewsletter($newsletter);
            $subscription->setSms($sms);
            $subscription->setRulesacceptation($rulesacceptation);
            $subscription->save();

            //Sending Mail
            $this->sendMail($post['email']);

            $this->messageManager->addSuccessMessage('Subscription done ! Thank you !');
        }
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setUrl('/contesthk/index/index');

        return $resultRedirect;
        
    }

    public function sendMail($email){
        $myvar = true;
        $emailTemplateVariables = array();
        $emailTempVariables['myvar'] = $myvar;

        $senderName = 'Antoine DEBOUT';
        $senderEmail = 'ant.debout@gmail.com';

        $postObject = new \Magento\Framework\DataObject();
        $postObject->setData($emailTempVariables);

        $sender = [
                    'name' => $senderName,
                    'email' => $senderEmail,
                    ];

        $transport = $this->_transportBuilder->setTemplateIdentifier('contesthk_subscription_template')
        ->setTemplateOptions(['area' => \Magento\Framework\App\Area::AREA_FRONTEND, 'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID])
        ->setTemplateVars(['data' => $postObject])
        ->setFrom($sender)
        ->addTo($email)
        ->setReplyTo('antoine.debout.95@gmail.com')            
        ->getTransport();               
        $transport->sendMessage();
    }
}