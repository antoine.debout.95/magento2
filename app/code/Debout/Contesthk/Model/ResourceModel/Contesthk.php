<?php

namespace Debout\Contesthk\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Contesthk Resource Model
 *
 * @author Antoine DEBOUT
 * @date 11/06/2020
 */
class Contesthk extends AbstractDb
{
    /**
     * Initialize resource
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('debout_contesthk', 'contesthk_id');
    }
}