<?php

namespace Debout\Contesthk\Model\ResourceModel\Contesthk;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Contesthk Resource Model Collection
 *
 * @author Antoine DEBOUT
 * @date 11/06/2020
 */
class Collection extends AbstractCollection
{
    /**
     * Initialize resource collection
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('Debout\Contesthk\Model\Contesthk', 'Debout\Contesthk\Model\ResourceModel\Contesthk');
    }
}