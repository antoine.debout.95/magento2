<?php

namespace Debout\Contesthk\Model;

use Magento\Cron\Exception;
use Magento\Framework\Model\AbstractModel;

/**
 * Contesthk Model
 * 
 * @author Antoine DEBOUT
 * @date 11/06/2020
 */
class Contesthk extends AbstractModel{
    /**
     * @var \Magento\Framework\Stdlib\DateTime
     */
    protected $_dateTime;

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Debout\Contesthk\Model\ResourceModel\Contesthk::class);
    }
}