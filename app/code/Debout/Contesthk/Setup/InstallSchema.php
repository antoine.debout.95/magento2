<?php
namespace Debout\Contesthk\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface{
    
    /**
     * Install function
     * @author Antoine DEBOUT
     * @date 11/06/2020
     * @return void
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup(); 
        if (!$setup->getConnection()->isTableExists($setup->getTable('pfay_contacts'))) {
            $table = $setup->getConnection()
                    ->newTable($setup->getTable('debout_contesthk'))
                    ->addColumn(
                        'contesthk_id',
                        \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                        null,
                        ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                        'ContestId'
                    )
                    ->addColumn(
                        'name',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        100,
                        ['nullable' => false, 'default' => 'name'],
                        'Name'
                    )
                    ->addColumn(
                        'firstname',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        100,
                        ['nullable' => false, 'default' => 'firstname'],
                        'Firstname'
                    )
                    ->addColumn(
                        'email',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        100,
                        ['nullable' => false, 'default' => 'email'],
                        'Email'
                    )
                    ->addColumn(
                        'phone',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        100,
                        ['nullable' => false, 'default' => 'phone'],
                        'Phone'
                    )
                    ->addColumn(
                        'newsletter',
                        \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
                        null,
                        ['nullable' => true, 'default' => 0],
                        'Newsletter'
                    )
                    ->addColumn(
                        'sms',
                        \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
                        null,
                        ['nullable' => true, 'default' => 0],
                        'SMS'
                    )
                    ->addColumn(
                        'rulesacceptation',
                        \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
                        null,
                        ['nullable' => false, 'default' => 0],
                        'RulesAcceptation'
                    )
                    ->addColumn(
                        'created_at',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                        null,
                        ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
                        'Created At'
                    )->addColumn(
                        'updated_at',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                        null,
                        ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
                        'Updated At'
                    )
                    ->setComment('Contest Subscription Table')
                    ->setOption('type', 'InnoDB')
                    ->setOption('charset', 'utf8');
            
            $setup->getConnection()->createTable($table);
        }
        $setup->endSetup();
    }
}